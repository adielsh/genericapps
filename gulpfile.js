const gulp = require('gulp');

// const babel = require('gulp-babel');
const jshint = require('gulp-jshint');
//const uglify = require('gulp-uglify');
// const gutil = require('gulp-util');
//const rename = require('gulp-rename');
// const webpack = require('webpack');
const webpackStream = require('webpack-stream');
const webpackConfig = require("./webpack.config.js");


gulp.task('lint', function () {
	return gulp.src('src/js/*.js')
		.pipe(jshint())
		.pipe(jshint.reporter('default'));
});

gulp.task('webpack', () => {
	return gulp.src('src/js/main.js')
		.pipe(webpackStream(webpackConfig))
		.pipe(gulp.dest('dist/js'));
});


gulp.task('default', ["lint", 'webpack'], () => {
	gulp.watch('dev/**/*.js', ["lint", 'webpack']);
});


gulp.task('production', ["lint", 'webpack'], () => {
	// return gulp.src('node_modules/bootstrap/dist/css/bootstrap.min.css')
	// 	.pipe(gulp.dest('dist/css'));
	// return gulp.src('node_modules/bootstrap/dist/fonts/*.*')
	// 	.pipe(gulp.dest('dist/fonts'));
});

gulp.task('css', () => {
	return gulp.src(['node_modules/bootstrap-rtl/dist/css/bootstrap-rtl.min.css',
		'node_modules/bootstrap-rtl/dist/css/bootstrap-rtl.css.map',
		'node_modules/bootstrap/dist/css/bootstrap.min.css'])
		.pipe(gulp.dest('dist/css'));
});