import * as Backbone from "backbone";
import DomManager from "./DomManager";
import {LoginView} from "./User/view/LoginView";
import {RegisterView} from "./User/view/RegisterView";
import {ForgotPassView} from "./User/view/ForgotPassView";
import {MainPageView} from "./User/view/MainPageView";
import {CreateAppView} from "./User/view/CreateAppView";
import {HeaderView} from "./User/view/HeaderView";
// import Vue from 'vue'
// import VueRouter from 'vue-router'
//
// Vue.use(VueRouter)


class AppRouter extends Backbone.Router {

    constructor() {
        super({
            routes: {
                "": "home",
                "register": "register",
                "forgotPass": "forgotPass",
                "mainPage": "mainPage",
                "appContainer": "appContainer",
                "createApp/:id": "createApp",
               "createApp": "createApp"

                // "singup": "home",
            }
        });
        // this.eventBus = _.extend({}, Backbone.Events);
        this.headerView = new HeaderView(this.eventBus);
        this.domManager.setInHeader(this.headerView);

    }

    // execute(callback, args) {
    //     if (confirm("זה לא ישמור!!!")) {
    //         super.execute(callback, args);
    //     }
    // }


    initialize() {
        this.domManager = new DomManager();
        this.domManager.render();
    }


    home() {

        this.view = new LoginView();
        this.domManager.setInContent(this.view);
    }

    register() {
        this.view = new RegisterView();
        this.domManager.setInContent(this.view);
    }

    forgotPass() {
        this.view = new ForgotPassView();
        this.domManager.setInContent(this.view);
    }

    mainPage() {
        this.view = new MainPageView(this.eventBus);
        this.domManager.setInContent(this.view);

    }

    createApp(id) {
        this.view = new CreateAppView(id);
        this.domManager.setInContent(this.view);

    }
    //
    // createApp2() {
    //     this.view = new CreateAppView();
    //     this.domManager.setInContent(this.view);
    //
    // }

// export default class AppRouter extends VueRouter {
//
//     constructor() {
//         console.log("start router constructor");
//         super({
//             routes: {
//                 "": "home",
//                 "register":"register",
//                 "forgotPass":"forgotPass",
//                 "mainPage":"mainPage",
//                 "appContainer":"appContainer"
//
//                 // "singup": "home",
//             }
//         });
//     }


}
;
let router = null;
function getRouter() {
    if (!router)
        router = new AppRouter();
    return router;
}
export default getRouter;


function toTop() {
    $('html,body').scrollTop(0);
}

