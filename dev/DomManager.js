var Backbone = require("backbone");
import HeaderView from "./User/view/HeaderView"

export default class DomManager extends Backbone.View {

	constructor() {
		super({
			el: "body"
		});
	}

	initialize()
	{


	//	this.$el.find("#header").html((this.header = new HeaderView()).el);
		this.content = this.$el.find("#content");
		this.header = this.$el.find("#header");
		this.nowView = null;
		DomManager.single = this;


	}

		render() {

			//this.$el.find("#header").html((this.header = new HeaderView()).render().el);
			return this;
	}
    //let that=this;



	setInHeader(view){
		view.render();
		this.header.html(view.el);
		//this.header.hide()

	}



	setInContent(view) {


		if(view.id=="loginPage")
		{
			this.header.hide()
		}

		view.render();
		this.content.html(view.el);
		if (this.nowView != null) {
			this.nowView.remove();
			this.nowView.trigger("removeFromDom");
		}
		this.nowView = view;
		this.nowView.trigger("setInDom");
	}
};
DomManager.single = null;