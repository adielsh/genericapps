/**
 * Created by אלעד on 03 אוגוסט 2016.
 */
import $ from "jquery";
import Backbone from "backbone";

import AppRouter from "./AppRouter";

require('bootstrap');

//const BASE_URL = "api.php?side=manager";
let appRouter;
//export default appRouter;
$(document).ready(()=> {
    console.log(AppRouter);
    appRouter = AppRouter();
    window.app = appRouter;
    Backbone.history.start();
});

export default appRouter;