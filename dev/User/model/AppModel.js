/**
 * Created by Adiel on 13/10/2016.
 */
/**
 * Created by Adiel on 13/10/2016.
 */

import * as Backbone from 'backbone';
//var app = app || {};//?


var NowApp = Backbone.Model.extend({
    initialize: function () {
    },
    defaults: {
        nowAppId: '-10',
        inProccess: '0',
    }

});

var AppModel = Backbone.Model.extend({
    initialize: function () {
    },
    defaults: {
        serial: '',
        name: 'ללא שם',
        status: 'ללא סטטוס',
        title: 'Generics !',
        logo: 'img/item/application.png',
        description: '',
        color_title_text: "#000000",
        color_title_background: "#ff0000",
        title_bold: "true",
        title_font: "arial",
        color_message_text: "#00ff00",
        color_message_background: "#ffffff",
        message_bold: "false",
        message_font: "arial",
        type: '0',  //that for three types of Tips System
        frequency: '0',  //daily-1, weekly-2, monthly-3
        image_background_location: "http://images.freeimages.com/images/home-grids/647/red-book-1420913.jpg",
        google_ads_id: "0123456",
        show_ads: "true",
        design_type: '1',
    }

});

var app1 = new AppModel({

    serial: '1',
    name: 'אפליקציה 1',
    status: 'בעריכה',
});
var app2 = new AppModel({
    serial: '2',
    name: 'אפליקציה 2',
    status: 'הועלה לחנות',
});
var app3 = new AppModel({
    serial: '3',
    name: 'אפליקציה 3',
    status: 'בטיפול',
});
var app4 = new AppModel({
    serial: '3',
    name: 'אפליקציה 4',
    status: 'בטיפול',
});


//'AppsCollection' is an instance of collection
var AppsCollection = Backbone.Collection.extend({
    model: AppModel  //model 'App' is specified by using model property

});
var nowApp = new NowApp();
var myAppsCollection = new AppsCollection();   //'myAppsCollection' is instance of the collection
myAppsCollection.add(app1);
myAppsCollection.add(app2);
myAppsCollection.add(app3);
myAppsCollection.add(app4);
let AppsGlobModel = {myAppsCollection: myAppsCollection, AppModel: AppModel, nowApp: nowApp}
export default AppsGlobModel;

