/**
 * Created by Adiel on 13/10/2016.
 */
/**
 * Created by Adiel on 13/10/2016.
 */

import * as Backbone from 'backbone';
//var app = app || {};//?

var LinkModel = Backbone.Model.extend({
    initialize: function(){
        console.log("created Link model");


    },
    defaults: {
        serial:'',
        text:'',
        link: '',
        image:'',
    }

});

var LinkCollection = Backbone.Collection.extend({
    model: LinkModel  //model 'App' is specified by using model property

});

var myLinksCollection = new LinkCollection();
let LinksGlobModel = {myLinksCollection: myLinksCollection}
export default LinksGlobModel;

