/**
 * Created by Adiel on 13/10/2016.
 */
/**
 * Created by Adiel on 13/10/2016.
 *
 *
 *
 */


import * as Backbone from 'backbone';
//var app = app || {};//?



var UserModel = Backbone.Model.extend({
    initialize: function(){
    },
    defaults: {
        name: 'אין שם',
        family:'',
        email:'',
        password:'',
        image: 'SomePath',
        userApps:'',
    },

});


var user1  = new UserModel({
    name: 'יעקב',
    family:'',
    email: 'asdf@a.com',
    password:'123',
    image: 'SomePath',
    userApps:'',
});
var user2  = new UserModel({
    name: 'ששון',
    family:'',
    email: 'bfad@a.com',
    password:'234',
    image: 'SomePath',
    userApps:'',
});
var user3 = new UserModel({
    name: 'שמחה',
    family:'',
    email: 'fsfs@a.com',
    password:'234',
    image: 'SomePath',
    userApps:'',
});
var nowUser = new UserModel({
    name: 'שמחה',
    family:'מועלם',
    email: 'fsfs@a.com',
    password:'234',
    image: 'SomePath',
    userApps:'',
});


var UsersCollection = Backbone.Collection.extend({
    model: UserModel  //model 'App' is specified by using model property

});

var myUsersCollection = new UsersCollection();   //'myAppsCollection' is instance of the collection
myUsersCollection.add(user1);
myUsersCollection.add(user2);
myUsersCollection.add(user3);
let UsersGlobModel = {myUsersCollection: myUsersCollection, UserModel: UserModel ,nowUser:nowUser}
export default UsersGlobModel;

