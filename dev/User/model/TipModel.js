/**
 * Created by Adiel on 13/10/2016.
 */
/**
 * Created by Adiel on 13/10/2016.
 */

import * as Backbone from 'backbone';
import {default as moment} from 'moment';


//var app = app || {};//?

var TipModel = Backbone.Model.extend({
    initialize: function () {
        console.log("created DateTipModel model");
        // this.set ("nowDate",nowDate = moment().subtract(10, 'days').calendar());

    },
    defaults: {
        appID: '',
        date: '',
        text: '',
        type: '',
        serial: '',
    }

});

// var ProcessTipModel = Backbone.Model.extend({
//     initialize: function () {
//         console.log("created ProcessTipModel model");
//
//
//     },
//     defaults: {
//         // appID: '',
//         serial: '',
//         text: '',
//         type: '',
//     }
//
// });


var TipCollection = Backbone.Collection.extend({
    model: TipModel  //model 'App' is specified by using model property

});


var myTipCollection = new TipCollection();


let TipsGlobModel = {
    myTipCollection: myTipCollection,
    TipModel: TipModel,
}
// let TipsGlobModel = {myTipCollection: myTipCollection, TipModel:TipModel}
export default TipsGlobModel;

