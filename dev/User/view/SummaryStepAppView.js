/**
 * Created by Lior on 03-Nov-16.
 */
import * as Backbone from "backbone";
import * as Handlebars from "handlebars/dist/handlebars"
import mycollection from "../model/AppModel"
import Helper from "../../jqueryMore";
import TipsGlobModel from "../model/TipModel"
import {default as swal} from 'sweetalert2';
import AppsGlobModel from "../model/AppModel"


export default class SummaryStepAppView extends Backbone.View {
    constructor(appModel, tempAppModel, doesNewApp) {
        super({
            events: {}
        });
        this.$el.html(Helper.uploadTpl("summaryStepApp"));
        this.$el.find("#appName").text(tempAppModel.get('name'));
        this.$el.find("#appDescription").text(tempAppModel.get('description'));
        this.$el.find("#appName").text(tempAppModel.get('name'));
        this.saveBtn = this.$el.find("#saveBtn")

        switch (tempAppModel.get('type')) {
            case '1':
                this.$el.find("#maslul").text("מסלול ליווי אישי");
                break;
            case '2':
                this.$el.find("#maslul").text("מסלול תהליך אישי");

                break;
            case '3':
                this.$el.find("#maslul").text("מסלול תלוי תאריך");
                break;
            default:
                break;
        }

        switch (tempAppModel.get('frequency')) {
            case '1':
                this.$el.find("#frequency").text("יומי");
                break;
            case '2':
                this.$el.find("#frequency").text("שבועי");

                break;
            case '3':
                this.$el.find("#frequency").text("חודשי");
                break;
            default:
                break;
        }


        switch (tempAppModel.get('design_type')) {


            case "1":

                this.$el.find("#firstOptionStyleTitleDiv").removeClass();
                this.$el.find("#firstOptionStyleTitleDiv").addClass("firstTitle");


                this.$el.find("#firstOptionStyleTipDiv").removeClass();
                this.$el.find("#firstOptionStyleTipDiv").addClass("firstTip");


                this.$el.find("#firstOptionStyleAdsDiv").removeClass();
                this.$el.find("#firstOptionStyleAdsDiv").addClass("firstAds");

                break;

            case "2":

                this.$el.find("#firstOptionStyleTitleDiv").removeClass();
                this.$el.find("#firstOptionStyleTitleDiv").addClass("secTitle");


                this.$el.find("#firstOptionStyleTipDiv").removeClass();
                this.$el.find("#firstOptionStyleTipDiv").addClass("secTip");


                this.$el.find("#firstOptionStyleAdsDiv").removeClass();
                this.$el.find("#firstOptionStyleAdsDiv").addClass("secAds");


                break;
            case "3":
                this.$el.find("#firstOptionStyleTitleDiv").removeClass();
                this.$el.find("#firstOptionStyleTitleDiv").addClass("thirdTitle");


                this.$el.find("#firstOptionStyleTipDiv").removeClass();
                this.$el.find("#firstOptionStyleTipDiv").addClass("thirdTip");


                this.$el.find("#firstOptionStyleAdsDiv").removeClass();
                this.$el.find("#firstOptionStyleAdsDiv").addClass("thirdAds");

                break;

            default:
                break;
        }


        this.$el.find("#firstOptionStyleTitleDiv").html(tempAppModel.get('title'))
        this.$el.find("#firstOptionStyleTitleDiv").css(({'color': tempAppModel.get('color_title_text')}))
        this.$el.find("#firstOptionStyleTitleDiv").css(({'background-color': tempAppModel.get('color_title_background')}))
        this.$el.find("#firstOptionStyleTitleDiv").css({'font-weight': tempAppModel.get('title_bold')})
        this.$el.find("#firstOptionStyleTipDiv").css(({'color': tempAppModel.get('color_message_text')}))
        this.$el.find("#firstOptionStyleDiv").css({'background-color': tempAppModel.get('color_message_background')})
        this.$el.find("#firstOptionStyleTipDiv").css({'font-weight': tempAppModel.get('message_bold')})
        this.$el.find("#firstOptionStyleTipDiv").css({'font-family': tempAppModel.get('message_font')})


        if (TipsGlobModel.myTipCollection.length != 0) {

            TipsGlobModel.myTipCollection.each((obj) => {
                console.log(obj.toJSON())
                if (obj.get('date') == '')
                    this.$el.find("#tipsTable").append("<tr><td>" + obj.get('serial') + "</td><td>" + obj.get('text') + "</td></tr>")
                else  this.$el.find("#tipsTable").append("<tr><td>" + obj.get('date') + "</td><td>" + obj.get('text') + "</td></tr>")


            })
        }

        this.$el.find("#logo").attr('src', tempAppModel.get('logo'));

        this.saveBtn.on('click', ()=> {
            this.lastStepApp(appModel, tempAppModel, doesNewApp);
        })


    }

    render() {

    }


    lastStepApp(appModel, tempAppModel, doesNewApp) {
        // this.lastStepAppView = new LastStepAppView();
        // this.inner_content.html(this.lastStepAppView.el);
        // if (this.doesNewApp == true) {
        //     AppsGlobModel.myAppsCollection.add(this.currentAppModel);
        // }
        //  AppsGlobModel.nowApp.set({'nowAppId': '-10'})
        //AppsGlobModel.nowApp.set({'inProccess': '0'})
        //this.renderApp();
        //  Backbone.history.loadUrl('mainPage');

        if (tempAppModel.get('name') == '') {

            swal({
                title: 'לא הכנסת שם!',
                text: 'הכנס שם בבקשה',
                timer: 500
            }).then(
                function () {
                },
                // handling the promise rejection
                function (dismiss) {
                    if (dismiss === 'timer') {
                        console.log('I was closed by the timer')
                    }
                }
            )
        }
        else {


            if (doesNewApp == false)
                appModel.set(tempAppModel.attributes);
            else {
                AppsGlobModel.myAppsCollection.add(tempAppModel)
                console.log("savedOnCollection")
                swal(
                    'אפליקציה נוספה בהצלחה',
                    '',
                    'success'
                )
            }

            window.app.navigate("mainPage", {trigger: true, replace: true})

            swal(
                'עריכה בוצעה בהצלחה!',
                '' +
                '!',
                'success'
            )
        }
    }


}
