/**
 * Created by Adiel on 09/10/2016.
 */
/**
 * Created by Adiel on 09/10/2016.
 */
import * as Backbone from "backbone";
import {default as swal} from 'sweetalert2';
import 'bootstrap';

// import Vue from 'vue'
import AppsGlobModel from "../model/AppModel"
import TipsGlobModel from "../model/TipModel"
//import AppModel from "../model/AppModel"
import ApplicationView from "../view/ApplicationView"
import Helper from "../../jqueryMore";


export class MainPageView extends Backbone.View {
    constructor(eventBus) {
        //    swal('adsas')
        super({});
        this.eventBus = eventBus;
        this.appView = [];
        this.id = "mainPage"
        // this.$el.find("#content")
        this.$el.html(Helper.uploadTpl("mainPage"));
        this.appContainer = this.$el.find("#app-container");
        this.addAppButton = this.$el.find("#addAppButton");
        AppsGlobModel.nowApp.set({'inProccess': '0'})
        this.searchByName = this.$el.find("#searchByName")
        this.nameSearchBtn = this.$el.find("#nameSearchBtn")
        this.statusSearchBtn = this.$el.find("#statusSearchBtn")
        this.searchByStatus = this.$el.find("#searchByStatus")
        // this.$el.find("#addAppButton").click(function(){
        //     //  goTo("mainPage");
        // });

        this.iteratedAppArray = AppsGlobModel.myAppsCollection;

        // $("#addAppButton").on('click',function() {
        //
        //         // do operation
        //         Backbone.history.loadUrl('createApp');
        //   })


        // this.eventBus.on('addBtnPressed', this.newLocation, this);
        //  Backbone.on('mainBtnPressed', this.newLocation, this);
        // this.eventBus.on('mainBtnPressed', this.newLocation, this);
        this.renderApp(AppsGlobModel.myAppsCollection);
        this.renderAppOnNameSearch()
        this.renderAppOnStatusSearch()
        // this.addAppButton.click(() => {
        //     this.appContainer.html("<br>");
        //     AppsGlobModel.mycollection.add(new AppsGlobModel.AppModel({
        //         name: 'עוד אחת',
        //         status: 'בפיתוח',
        //     }));
        //     this.renderApp();
        // });


        // TipsGlobModel.myTipCollection.on("remove", function () {
        //     this.showProcessTipToCollection()
        //
        // }.bind(this));


    }


    renderApp(appColl) {
        this.appContainer.html('')
        let
            that = this;
        var i = 0;

        appColl.each((appModel)=> {
            that.appView[i] = new ApplicationView(appModel);
            //appView[i].render();
            //Also can call from here
            that.appContainer.append(that.appView[i].$el);
            console.log(i)

            i++;
        })
    }

// renderAppOnNameSearch() {
//     let that = this;
//     this.nameSearchBtn.on('click', ()=> {
//
//         this.appContainer.html('')
//
//         if (this.searchByName.val() == '') {
//
//             this.renderApp()
//         }
//         else {
//             var i = 0;
//             AppsGlobModel.myAppsCollection.where({name: this.searchByName.val()}).forEach((appModel)=> {
//                 that.appView[i] = new ApplicationView(appModel);
//                 that.appContainer.append(that.appView[i].$el);
//                 // console.log(j)
//                 i++;
//             })
//         }
//     })
//
// }

    renderAppOnNameSearch() {
        let that = this;

        this.searchByName.on('keyup', ()=> {
                this.appContainer.html('')
                this.towFilters()
                // if (this.searchByName.val() == '') {
                //
                //     if (this.$el.find("#searchByStatus option:selected").val() == 0)
                //         this.renderApp(this.iteratedAppArray = AppsGlobModel.myAppsCollection)
                //     else  this.renderApp(this.iteratedAppArray)
                // }
                // else {

                // }
            }
        )
    }

    towFilters() {
        var that = this;
        var i = 0;
        this.iteratedAppArray
            .filter((model)=> {
                return (model.get('name').indexOf(that.searchByName.val()) > -1)
            })
            .filter((model)=> {

                if (this.$el.find("#searchByStatus option:selected").val() == 0)
                    return true;
                else
                    return (model.get('status').indexOf(that.$el.find("#searchByStatus option:selected").text()) > -1)
            })
            .forEach((appModel)=> {
                that.appView[i] = new ApplicationView(appModel);
                that.appContainer.append(that.appView[i].$el);
                // console.log(j)
                i++;
            })
    }

    renderAppOnStatusSearch() {
        this.$el.on('change', "#searchByStatus", ()=> {
            this.appContainer.html('')
            this.towFilters()
            //         // this.renderApp()
            //         console.log("sdfsdfd")
            //         var i = 0;
            //         this.iteratedAppArray.filter(function (model) {
            //             console.log("2423234")
            //             return (model.get('status').indexOf(this.$el.find("#searchByStatus option:selected").text()) > -1)
            //         }.bind(this)).filter;
            //
            //         if (this.iteratedAppArray.length != 0) {
            //             console.log("גדול מ0")
            //             this.appContainer.html('')
            //             this.iteratedAppArray.forEach((appModel)=> {
            //                 that.appView[i] = new ApplicationView(appModel);
            //                 that.appContainer.append(that.appView[i].$el);
            //                 // console.log(j)
            //                 i++;
            //             })
            //         }
            //         else if (this.iteratedAppArray.length == 0) {
            //             this.appContainer.html('')
            //
            //         }
            //
            //         // if (AppsGlobModel.myAppsCollection.filter(function (model) {
            //         //         return (model.get('status').indexOf(this.$el.find("#searchByStatus option:selected").text()) > -1)
            //         //     }).length == 0)
            //         // {
            //         //
            //         //
            //         //
            //         // }
            //
            //         if (this.$el.find("#searchByStatus option:selected").val() == 0) {
            //             if (this.searchByName.val() == '')  this.renderApp(this.iteratedAppArray = AppsGlobModel.myAppsCollection)
            //             else this.renderApp(this.iteratedAppArray)
            //         }
            //
            //
        })
        //
        //
    }


//
// newLocation () {
//
//     // Backbone.history.loadUrl('mainPage');
//
//
//     alert("התראה בצעד חכם של טריגר בקבון מההדר")
// };

    render() {


    }

}








