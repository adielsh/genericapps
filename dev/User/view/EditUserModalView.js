/**
 * Created by Adiel on 09/10/2016.
 */
//import * as Backbone from "backbone-modal";
import Helper from "../../jqueryMore";
import * as Backbone from "backbone";
import UsersGlobModel from "../model/UserModel"


export class EditUserModalView extends Backbone.View {


    constructor() {
        super({

            events: {
                "hidden.bs.modal": "remove",
                "click #saveButton": "onSaveButtonClick",

            }
        });
        this.$el.html(Helper.uploadTpl("editUser"));
        // this.template = uploadTpl(template);
        // this.$el.html(this.template);
        this.modal = this.$el.find(".modal");


        this.nameTextinput = this.$el.find("#nameTextinput").val(UsersGlobModel.nowUser.get('name'));
        this.familyTextinput = this.$el.find("#familyTextinput").val(UsersGlobModel.nowUser.get('family'));
        this.mailTextinput = this.$el.find("#mailTextinput").val(UsersGlobModel.nowUser.get('email'));
        this.passwordTextinput = this.$el.find("#passwordTextinput").val(UsersGlobModel.nowUser.get('password'));
        this.againPasswordTextinput = this.$el.find("#againPasswordTextinput");
        //this.saveButton = this.$el.find("#saveButton");


        // this.saveButton.on("click",function () {
        //
        //     UsersGlobModel.nowUser.set({'name':this.nameTextinput.val()})
        //    // UsersGlobModel.nowUser.set({'name':this.nameTextinput.val(),'family':this.familyTextinput.val(),'email':this.mailTextinput.val(),'password': this.passwordTextinput.val()})
        // })


        this.validate = function () {

            this.passwordTextinput.onchange = this.validatePassword;
            this.againPasswordTextinput.onkeyup = this.validatePassword;


            this.validatePassword = function () {
                if (this.againPasswordTextinput.value != this.againPasswordTextinput.value) {
                    this.againPasswordTextinput.setCustomValidity("Passwords Don't Match");
                } else {
                    this.againPasswordTextinput.setCustomValidity('');
                }
            }

        }
        ;

    }

    onSaveButtonClick() {
        this.validate();
        UsersGlobModel.nowUser.set({name: this.nameTextinput.val()})

        console.log("save user data")
        console.log(UsersGlobModel.nowUser.get('name'))

// UsersGlobModel.nowUser.set({'name':this.nameTextinput.val(),'family':this.familyTextinput.val(),'email':this.mailTextinput.val(),'password': this.passwordTextinput.val()})


    }


    render() {

    }

    show() {
        $("body").append(this.el);
        this.modal.modal("show");
        this.modal.one("hidden.bs.modal", this.remove.bind(this));
    }

    close() {
        this.modal.modal("hide");
    }

    remove() {
        Backbone.View.prototype.remove.call(this);
        this.trigger("close", this);
    }


}
export default EditUserModalView;

//"use strict";

//
// var forgot  = function () {
//     var BASE_URL = "api.php?";
//     var form = this.$el.find("#forget_form");
//     form.on("submit", sendForm);
//     function sendForm() {
//         //
//         form.disabled();
//         $.ajax({
//             data: form.getInputObj(),
//             url: "api.php?action=customer&fun=forget",
//             method: "POST",
//             success: function success(data) {
//                 onSuccess();
//             },
//             fail: onFail
//         });
//         return false;
//     }
//
//     function onSuccess(data) {
//         form.replaceWith($('<div class="alert alert-success" role="alert">סיסמא נשלחה אלייך במייל</div>'));
//     }
//
//     function onFail(data) {
//         alert("שגיאת שרת!");
//     }
// }
