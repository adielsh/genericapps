import * as Backbone from "backbone";
import mycollection from "../model/AppModel"
import Helper from "../../jqueryMore";
import FirstStepAppView from "../view/FirstStepAppView"
import SecStepAppView from "../view/SecStepAppView"
import ThirdStepAppView from "../view/ThirdStepAppView"
import ForthStepAppView from "../view/ForthStepAppView"
import SummaryStepAppView  from "../view/SummaryStepAppView"
import DesignStepAppView  from "../view/DesignStepAppView"
import LastStepAppView from "./SummaryStepAppView"
import AppsGlobModel from "../model/AppModel"
import TipsGlobModel from "../model/TipModel"
import {default as swal} from 'sweetalert2';
import appRouter from "../../app"
import {default as moment} from 'moment';


export class CreateAppView extends Backbone.View {
    constructor(serial) {
        super({});

        this.currentAppModel = null
        this.tempModel = new AppsGlobModel.AppModel({
            serial: '',
            name: '',
            status: '',
        })
        this.$el.html(Helper.uploadTpl("createApp"));
        this.doesNewApp = false;
        //  _.bindAll(this, 'contentChanged');
        this.inner_content = this.$el.find("#inner_content");

        this.secStepView = null;
        this.thirdStepView = null;
        this.forthStepAppView = null;
        this.designStepAppView = null;

        this.prevBtn = this.$el.find("#prevBtn")
        this.nextBtn = this.$el.find("#nextBtn")


        let that = this;
        //this.currentAppModel = new AppsGlobModel.AppModel({appId: AppsGlobModel.myAppsCollection.length + 1});

        if (serial == null) {
            console.log("id is null")
            this.tempModel.set({serial: AppsGlobModel.myAppsCollection.length + 1})
            this.doesNewApp = true;
            this.firstStepApp();
        }

        else {

            // this.realModel = this.model;
            // this.model = this.realModel.clone();
            //

            AppsGlobModel.myAppsCollection.each((appModel)=> { //  or edit exsiting app

                // this.model = appModel;
                if (appModel.get('serial') == serial) {
                    console.log("מצאתי את הסריאל!")

                    that.currentAppModel = appModel;
                    that.tempModel = that.currentAppModel;
                    that.tempModel = that.currentAppModel.clone();


                    this.firstStepApp();
                    // that.tempModel.set({name:"dfs"})
                    // console.log(that.currentAppModel.get('name'))
                    // console.log(that.tempModel.get('name'))


                }
            })
        }

        console.log(this.currentAppModel)


        // if (AppsGlobModel.nowApp.get('nowAppId') < 0) {   //asked if we are redirecting from create new one
        //     this.doesNewApp = true;
        //     this.currentAppModel = new AppsGlobModel.myAppsCollection.add(new AppsGlobModel.AppModel({
        //         appId: AppsGlobModel.myAppsCollection.length + 1,
        //         name: 'עוד אחת',
        //         status: 'בפיתוח',
        //     }))
        //
        //     that.firstStepApp(that.currentAppModel);
        //
        // }
        //
        //
        // else {
        //     AppsGlobModel.myAppsCollection.each((appModel)=> { //  or edit exsiting app
        //         // this.model = appModel;
        //         if (appModel.get('appId') == id) {
        //
        //             that.currentAppModel = appModel;
        //             //
        //             // that.currentAppModel.set({
        //             //     'appId': appModel.get('appId'),
        //             //     'name': appModel.get('name'),
        //             //     'status': appModel.get('status'),
        //             //     'description': appModel.get('description')
        //             // })
        //
        //             that.firstStepApp(that.currentAppModel);
        //         }
        //     });
        // }


    }

    get events() {
        return {
            "click #firstBtn": "firstStepApp",
            "click #secondBtn": "secStepApp",
            "click #thirdBtn": "thirdStepApp",
            "click #forthBtn": "forthStepApp",
            "click #designBtn": "designStepApp",
            "click #summaryBtn": "summaryStepApp",
            // "click #lastBtn": "lastStepApp",
            "change input.content": "contentChanged"
        }
    }


    newLocation() {
        // Backbone.history.loadUrl('mainPage');
    }
    ;


// data() {
//     mycollection: mycollection
// };

    firstStepApp() {
        //  AppsGlobModel.nowApp.set({'inProccess': '1'})   //mark that now we are in proccess, its help to sweetalert
        this.firstStepView = new FirstStepAppView(this.tempModel, this.doesNewApp);
        this.inner_content.html(this.firstStepView.el);
        this.nextBtn.show()
        this.prevBtn.hide()

        this.nextBtn.on('click', ()=> {
            this.secStepApp()
        })


        // if (AppsGlobModel.nowApp.get('nowAppId') < 0) {   //asked if we are redirecting from create new one
        //     this.firstStepView = new FirstStepAppView(this.currentAppModel);
        //     this.inner_content.html(this.firstStepView.el);
        //     this.doesNewApp = true;
        // }
        // else {
        //     AppsGlobModel.myAppsCollection.each((appModel)=> { //  or edit exsiting app
        //         if (appModel.get('appId') == AppsGlobModel.nowApp.get('nowAppId')) {
        //             this.firstStepView = new FirstStepAppView(appModel);
        //             this.inner_content.html(this.firstStepView.el);
        //         }
        //
        //     })
        //
        // }


    }

    secStepApp() {
        // if(this.secStepView ==null)
        //  {
        this.secStepView = new SecStepAppView(this.tempModel);
        // }
        this.inner_content.html(this.secStepView.el);
        this.nextBtn.show()
        this.prevBtn.show()

        this.nextBtn.on('click', ()=> {
            this.forthStepApp()
        })
        this.prevBtn.on('click', ()=> {
            this.firstStepApp()
        })

    }

    thirdStepApp() {
        //    if(this.thirdStepView ==null)
        // {
        this.thirdStepView = new ThirdStepAppView(this.tempModel);
        //}
        this.inner_content.html(this.thirdStepView.el);
        this.nextBtn.show()
        this.prevBtn.show()
        this.nextBtn.on('click', ()=> {
            this.forthStepApp()
        })
        this.prevBtn.on('click', ()=> {
            this.secStepApp()
        })
    }

    forthStepApp() {
        // if(this.forthStepAppView ==null)
        // {
        this.forthStepAppView = new ForthStepAppView(this.tempModel);
        //}
        console.log(this.forthStepAppView.$el)
        this.inner_content.html(this.forthStepAppView.el);
        this.nextBtn.show()
        this.prevBtn.show()

        this.nextBtn.on('click', ()=> {
            this.designStepApp()
        })
        this.prevBtn.on('click', ()=> {
            this.secStepApp()
        })
    }

    designStepApp() {
        // if(this.forthStepAppView ==null)
        // {
        this.designStepAppView = new DesignStepAppView(this.tempModel);
        //}
        //console.log(this.forthStepAppView.$el)
        this.inner_content.html(this.designStepAppView.el);
        this.nextBtn.show()
        this.prevBtn.show()
        this.nextBtn.on('click', ()=> {
            this.summaryStepApp()
        })
        this.prevBtn.on('click', ()=> {
            this.forthStepApp()
        })
    }

    summaryStepApp() {
        this.summaryStepAppView = new SummaryStepAppView(this.currentAppModel, this.tempModel, this.doesNewApp);
        //}
        //console.log(this.forthStepAppView.$el)
        // this.nextBtn.hide()
        this.inner_content.html(this.summaryStepAppView.el);
    }


}

;


// var BindingView = Backbone.Epoxy.View.extend({
//     el: "#app-luke",
//     bindings: {
//         "input.first-name": "value:firstName,events:['keyup']",
//         "input.last-name": "value:lastName,events:['keyup']",
//         "span.first-name": "text:firstName",
//         "span.last-name": "text:lastName"
//     }
// });
//
// var view = new BindingView({model: bindModel})


//
// //old
// var indexView = Backbone.View.extend({
//
//
//    initialize: function(){
//      this.render();
//    },
//    el: '#content',
//    render: function(){
//        //console.log(this.$el);
//     $.get('tpl/FirstStep.html', function (data) {
//             template = _.template(data, {  });
//             this.$el.html(template);
//         }, 'html');
//
//
//   },
// });
//
//

// export class FirstAppView extends Backbone.View
// {
//     constructor()
//     {
//         super({});
//
//         this.template = Helper.uploadTpl("firstStepApp");
//         this.$el.html(this.template);
//

//
//     }
// }
//
//
// export class SecAppView extends Backbone.View
// {
//     constructor()
//     {
//         super({});
//         this.template = Helper.uploadTpl("secStepApp");
//         this.$el.html(this.template);
//
//     }
// }