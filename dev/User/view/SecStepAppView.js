import * as Backbone from "backbone";
import * as Handlebars from "handlebars/dist/handlebars"
import mycollection from "../model/AppModel"
import Helper from "../../jqueryMore";
import TipsGlobModel from "../model/TipModel"


export default class SecStepApp extends Backbone.View {
    constructor(appModel) {
        super({
            events: {
                //  "keyup .nameField": "nameChanged",
            }
        });

        this.$el.html(Helper.uploadTpl("secStepApp"));


        this.maslulDescription = this.$el.find("#maslulDescription")
        this.maslulSelectpicker = this.$el.find("#maslulSelectpicker")
        this.tadirutSelectpicker = this.$el.find("#tadirutSelectpicker")
        this.maslulDescription.prop('selectedIndex', 3);


        this.maslulDescription.html("")

        let that = this

        this.collectionChecking();
        this.selectBoxSet(appModel);

        this.$el.on('change', "#maslulSelectpicker", function () {
            var idx = this.selectedIndex;
            //   console.log(idx)
            switch (idx) {
                case 1:
                    that.maslulDescription.html("<h1>תזכירים לפי המצב האישי</h1><p>ניתן לדלג על תחילת התהליך</p>")
                    appModel.set({'type': '1'})

                    break;
                case 2:
                    that.maslulDescription.html("<h1>תזכירים  לפי ליווי אישי</h1><p> תזכירים מא' ועד ת' (לא ניתן לדלג על תחילת התהליך)</p>")
                    appModel.set({'type': '2'})

                    break;
                case 3:
                    that.maslulDescription.html("<h1>תזכיר תלוי תאריך </h1><p>שליחת תזכירים לפי התאריך </p>")
                    appModel.set({'type': '3'})

                    break;


            }
        });

        this.$el.on('change', "#tadirutSelectpicker", function () {
            var idx = this.selectedIndex;
            // console.log(idx)
            switch (idx) {
                case 1:

                    appModel.set({'frequency': '1'})
                    break;

                case 2:
                    appModel.set({'frequency': '2'})
                    break;

                case 3:
                    appModel.set({'frequency': '3'})
                    break;
            }
        });

    }

    render() {


    }


    setTadirutSelectpicker() {

    }

    setMaslulSelectpicker() {

    }

    collectionChecking() {
        if (TipsGlobModel.myTipCollection.length > 0) {
            console.log("collection added!!")

            this.tadirutSelectpicker.attr("disabled", true);
            this.maslulSelectpicker.attr("disabled", true);
        }


    };

    selectBoxSet(appModel) {
        switch (appModel.get('type')) {
            case '1':
                this.$el.find('#maslulSelectpicker option').eq(1).prop('selected', true);
                break;
            case '2':
                this.$el.find('#maslulSelectpicker option').eq(2).prop('selected', true);
                break;
            case '3':
                this.$el.find('#maslulSelectpicker option').eq(3).prop('selected', true);
                break;
            default:
                break;
        }

        switch (appModel.get('frequency')) {
            case '1':
                this.$el.find('#tadirutSelectpicker option').eq(1).prop('selected', true);
                break;
            case '2':
                this.$el.find('#tadirutSelectpicker option').eq(2).prop('selected', true);
                break;
            case '3':
                this.$el.find('#tadirutSelectpicker option').eq(3).prop('selected', true);
                break;
            default:
                break;
        }


    }


}


// $(function () {
//     $('div.product-chooser').not('.disabled').find('div.product-chooser-item').on('click', function () {
//         $(this).parent().parent().find('div.product-chooser-item').removeClass('selected');
//         $(this).addClass('selected');
//         $(this).find('input[type="radio"]').prop("checked", true);
//
//     });
// });