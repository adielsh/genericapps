/**
 * Created by Adiel on 30/10/2016.
 */
import * as Backbone from "backbone";
import * as Handlebars from "handlebars/dist/handlebars"
import TipsGlobModel from "../model/TipModel"
import AppGlobModel from "../model/AppModel"
import Helper from "../../jqueryMore";
import {default as moment} from 'moment';
import Sortable from 'sortablejs';
console.log(Sortable);
// require("sortablejs");

export default class ForthStepApp extends Backbone.View {
    constructor(appModel) {
        console.log(appModel)
        super({
            events: {

                //      "click #addTipBtn ": "addTip"
                //  "keyup .nameField": "nameChanged",
            }
        });
        // alert(moment(new Date(2011, 9, 16)))
        this.appModel = appModel;
        this.$el.html(Helper.uploadTpl("forthStepApp"));
        this.tipsContent = this.$el.find("#tipsContent")
        this.frequencyDiv = this.$el.find("#tadirutDiv")
        this.typeDiv = this.$el.find("#typeDiv")
        this.addTipBtn = this.$el.find("#addTipBtn")
        this.processTipViewArr = []
        this.dateTipViewArr = []
        this.currentTipModel = null;
        this.currentDateTipModel = null;
        this.frequency = null;
        this.frequencyChecking();

        this.deleteAllTipBtn = this.$el.find("#deleteAllTipBtn")

        let list = this.$el.find("#tipsContent")[0];
        // this.$el.find("#my-ui-list").disableSelection();

        let that = this
        Sortable.create(list, {
            handle: ".tile__title",
            onUpdate: function (evt/**Event*/) {
                window.item = evt; // the current dragged HTMLElement

                var item = evt.from


                that.resortTips()


            }
        })
    }


    showProcessTipToCollection() {
        this.tipsContent.html("")
        let i = 0;
        TipsGlobModel.myTipCollection.each((TipModel)=> {
            // processTipModel.set({
            //     serial: i+1
            // });
            this.processTipViewArr[i] = new ProcessTipView(TipModel);
            // var processTipView =  new ProcessTipView(processTipModel);


            this.tipsContent.append(this.processTipViewArr[i].$el);

            // this.processTipViewArr[i].divId = this.processTipViewArr[i].getMyIndex();
            // console.log(this.divId)

            // this.resortTips()
            i++
        })
    }

    resortTips() {
        this.processTipViewArr.forEach(function (tip) {
                console.log(tip.setMyNewIndex());
            }
        )
    }


    addProcessTipToCollection() {


// before add tip, we decide the tip serial, depend on the collection
        if (TipsGlobModel.myTipCollection.length == 0) {
            this.currentTipModel = new TipsGlobModel.TipModel({
                serial: 1
            });

        } else {
            this.currentTipModel = new TipsGlobModel.TipModel({
                serial: parseInt(TipsGlobModel.myTipCollection.at(TipsGlobModel.myTipCollection.length - 1).get('serial')) + 1
            });
        }

        TipsGlobModel.myTipCollection.add(this.currentTipModel)
    }


    showDateTipToCollection() {
        this.tipsContent.html("")
        let i = 0;
        TipsGlobModel.myTipCollection.each((TipModel)=> {
            // processTipModel.set({
            //     serial: i+1
            // });
            this.dateTipViewArr[i] = new DateTipView(TipModel);
            // var processTipView =  new ProcessTipView(processTipModel);
            this.tipsContent.append(this.dateTipViewArr[i].$el);
            i++
        })


        // addDateTipToCollection() {
        //     TipsGlobModel.myDateTipCollection.add(this.currentProcessTipModel)
        // }
    }


    addDateTipToCollection() {


// before add tip, we decide the tip serial, depend on the collection
        if (TipsGlobModel.myTipCollection.length == 0) {
            this.currentTipModel = new TipsGlobModel.TipModel({
                date: moment().format('DD/MM/YYYY')
            });
        }
        else {
            this.frequencyCheckingForDateTip()
        }
        TipsGlobModel.myTipCollection.add(this.currentTipModel)
    }


    frequencyChecking() {

        console.log(this.appModel.get('frequency'))
        console.log(this.appModel)
        //   console.log(this.appModel.get('tadirut')
        switch (this.appModel.get('frequency')) {
            case "1":
                this.frequencyDiv.html("תדירות: יומית");
                //  this.frequency = "1";
                this.tipTypeChecking();

                break;
            case "2":
                this.frequencyDiv.html("תדירות: שבועית")
                // this.frequency = "2";
                this.tipTypeChecking()
                break;

            case "3":
                this.frequencyDiv.html("תדירות: חודשית")
                //   this.frequency = "3";
                this.tipTypeChecking()
                break;

            default:
                this.frequencyDiv.html("לא נבחרה תדירות")
        }
    }


    frequencyCheckingForDateTip() {


        this.length = TipsGlobModel.myTipCollection.length
        //   console.log(this.appModel.get('tadirut')
        switch (this.appModel.get('frequency')) {
            case "1":
                this.currentTipModel = new TipsGlobModel.TipModel({
                    date: moment().add(1 * this.length, 'day').format('DD/MM/YYYY')
                });
                break;
            case "2":
                this.currentTipModel = new TipsGlobModel.TipModel({
                    date: moment().add(1 * this.length, 'week').format('DD/MM/YYYY')
                });

                break;
            case "3":
                this.currentTipModel = new TipsGlobModel.TipModel({
                    date: moment().add(1 * this.length, 'month').format('DD/MM/YYYY')
                });

                break;
            default:
        }
    }


    tipTypeChecking() {

        switch (this.appModel.get('type')) {

            case "1":
                this.typeDiv.html("מסלול נבחר: תהליך אישי")
                this.showProcessTipToCollection()
                this.addTipBtn.click(function () {
                    window.scrollTo(0, document.body.scrollHeight);
                    this.addProcessTipToCollection()
                    this.showProcessTipToCollection()
                }.bind(this))

                TipsGlobModel.myTipCollection.on("remove", function () {
                    this.showProcessTipToCollection()
                }.bind(this));


                this.$el.find("#deleteAllTipBtn").on('click', ()=> {

                    TipsGlobModel.myTipCollection.reset()
                    this.showProcessTipToCollection()

                })


                break;

            case "2":
                this.typeDiv.html("מסלול נבחר: ליווי אישי")
                this.showProcessTipToCollection()
                this.addTipBtn.click(function () {
                    this.addProcessTipToCollection()
                    this.showProcessTipToCollection()
                    window.scrollTo(0, document.body.scrollHeight);

                }.bind(this))

                TipsGlobModel.myTipCollection.on("remove", function () {
                    this.showProcessTipToCollection()
                }.bind(this));


                this.$el.find("#deleteAllTipBtn").on('click', ()=> {

                    TipsGlobModel.myTipCollection.reset()
                    this.showProcessTipToCollection()
                })


                break;

            case "3":
                this.typeDiv.html("מסלול נבחר: תזכיר תלוי תאריך")
                this.showDateTipToCollection()
                this.addTipBtn.click(function () {
                    this.addDateTipToCollection()
                    this.showDateTipToCollection()
                    window.scrollTo(0, document.body.scrollHeight);

                }.bind(this))

                TipsGlobModel.myTipCollection.on("remove", function () {
                    this.showDateTipToCollection()
                }.bind(this));


                this.$el.find("#deleteAllTipBtn").on("click", ()=> {

                    TipsGlobModel.myTipCollection.reset()
                    this.showDateTipToCollection()
                })


                break;

            default:
                this.typeDiv.html("עדיין לא נבחר מסלול")
        }

    }

    render() {
    }

}


class ProcessTipView extends Backbone.View {
    constructor(TipModel) {
        super({
            events: {
                //  "keyup .nameField": "nameChanged",
            }
        });
        var that = this

        this.processTipModel = TipModel;
        this.processTipSerial = this.$el.find("#processTipSerial")


        //  this.$el.html(Helper.uploadTpl("processTip"));
        let processTipModeJson = TipModel.toJSON();
        let han = Handlebars.compile(Helper.uploadTpl("processTip"));
        this.$el.append(han(processTipModeJson));


        // this.$el.find("#addTip").on('click', ()=> {
        //
        // })

        this.$el.find("#processTipContent").on('keyup', ()=> {

            this.$el.find("#tipCounter").text(60 - this.$el.find("#processTipContent").val().length + " תווים נשארו ")
        })


        this.$el.find("#processTipContent").on('input', function (e) {

            TipModel.set({text: that.$el.find("#processTipContent").val()})
        });


        this.$el.find("#processTipSerial").on('input', function (e) {

            this.processTipModel.set({serial: that.$el.find("#processTipSerial").val()})
        }.bind(this));


        this.$el.find("#deleteProcessTipContentButton").on('click', function (e) {

            this.processTipModel.destroy();
            this.remove();
        }.bind(this));

        //  this.processTipSerial.dblclick(function (e) {
        //          e.stopPropagation();
        //          var currentEle = this.processTipSerial;
        //          var value = this.processTipSerial.html();
        //          this.updateVal(currentEle, value);
        //
        //
        //
        //
        //      });
        //
        //
        // this.updateVal = function (currentEle, value) {
        //      $(currentEle).html('<input class="thVal" type="text" value="' + value + '" />');
        //      $(".thVal").focus();
        //      $(".thVal").keyup(function (event) {
        //          if (event.keyCode == 13) {
        //              $(currentEle).html($(".thVal").val().trim());
        //          }
        //      });
        //
        //      $(document).click(function () {
        //          $(currentEle).html($(".thVal").val().trim());
        //      });
        //  }


    }

    render() {

    }

    setMyNewIndex() {
        const index = this.$el.index() + 1;

        if (this.processTipModel.get("serial") != index) {
            this.processTipModel.set("serial", index);
            this.$el.find("#processTipSerial").val(index);
        }
    }

}


class DateTipView extends Backbone.View {
    constructor(dateTipModel) {
        super({
            events: {
                //  "keyup .nameField": "nameChanged",
            }
        });
        var that = this

        //   this.$el.html(Helper.uploadTpl("dateTip"));

        this.dateTipModel = dateTipModel

        let dateTipModeJson = this.dateTipModel.toJSON();
        let han = Handlebars.compile(Helper.uploadTpl("dateTip"));
        this.$el.append(han(dateTipModeJson));


        this.$el.find("#processTipContent").on('keyup', ()=> {

            this.$el.find("#tipCounter").text(60 - this.$el.find("#processTipContent").val().length + " תווים נשארו ")
        })


        this.$el.find("#dateTipContent").on('input', function (e) {

            dateTipModel.set({text: that.$el.find("#dateTipContent").val()})
        });


        this.$el.find("#dateTipSerial").on('input', function (e) {
            dateTipModel.set({date: that.$el.find("#dateTipSerial").val()})
        });

        this.$el.find("#deleteDateTipContentButton").on('click', function (e) {
            this.dateTipModel.destroy();
            this.remove();
        }.bind(this));


    }

    render() {
    }
}