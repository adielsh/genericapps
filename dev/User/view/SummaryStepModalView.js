/**
 * Created by Lior on 03-Nov-16.
 */
import * as Backbone from "backbone";
import * as Handlebars from "handlebars/dist/handlebars"
import mycollection from "../model/AppModel"
import Helper from "../../jqueryMore";
import TipsGlobModel from "../model/TipModel"


export default class SummaryStepModalView extends Backbone.View {
    constructor(appModel) {
        super({
            events: {}
        });
        this.$el.html(Helper.uploadTpl("summaryStepAppModal"));
        this.modal = this.$el.find(".modal");

        this.$el.find("#appName").text(appModel.get('name'));
        this.$el.find("#appDescription").text(appModel.get('description'));
        this.$el.find("#appName").text(appModel.get('name'));


        switch (appModel.get('type')) {
            case '1':
                this.$el.find("#maslul").text("מסלול ליווי אישי");
                break;
            case '2':
                this.$el.find("#maslul").text("מסלול תהליך אישי");
                break;
            case '3':
                this.$el.find("#maslul").text("מסלול תלוי תאריך");
                break;
            default:
                break;
        }

        switch (appModel.get('frequency')) {
            case '1':
                this.$el.find("#frequency").text("יומי");
                break;
            case '2':
                this.$el.find("#frequency").text("שבועי");

                break;
            case '3':
                this.$el.find("#frequency").text("חודשי");
                break;
            default:
                break;
        }


        switch (appModel.get('design_type')) {


            case "1":

                this.$el.find("#firstOptionStyleTitleDiv").removeClass();
                this.$el.find("#firstOptionStyleTitleDiv").addClass("firstTitle");


                this.$el.find("#firstOptionStyleTipDiv").removeClass();
                this.$el.find("#firstOptionStyleTipDiv").addClass("firstTip");


                this.$el.find("#firstOptionStyleAdsDiv").removeClass();
                this.$el.find("#firstOptionStyleAdsDiv").addClass("firstAds");

                break;

            case "2":

                this.$el.find("#firstOptionStyleTitleDiv").removeClass();
                this.$el.find("#firstOptionStyleTitleDiv").addClass("secTitle");


                this.$el.find("#firstOptionStyleTipDiv").removeClass();
                this.$el.find("#firstOptionStyleTipDiv").addClass("secTip");


                this.$el.find("#firstOptionStyleAdsDiv").removeClass();
                this.$el.find("#firstOptionStyleAdsDiv").addClass("secAds");


                break;
            case "3":
                this.$el.find("#firstOptionStyleTitleDiv").removeClass();
                this.$el.find("#firstOptionStyleTitleDiv").addClass("thirdTitle");


                this.$el.find("#firstOptionStyleTipDiv").removeClass();
                this.$el.find("#firstOptionStyleTipDiv").addClass("thirdTip");


                this.$el.find("#firstOptionStyleAdsDiv").removeClass();
                this.$el.find("#firstOptionStyleAdsDiv").addClass("thirdAds");

                break;

            default:
                break;
        }


        this.$el.find("#firstOptionStyleTitleDiv").html(appModel.get('title'))
        this.$el.find("#firstOptionStyleTitleDiv").css(({'color': appModel.get('color_title_text')}))
        this.$el.find("#firstOptionStyleTitleDiv").css(({'background-color': appModel.get('color_title_background')}))
        this.$el.find("#firstOptionStyleTitleDiv").css({'font-weight': appModel.get('title_bold')})
        this.$el.find("#firstOptionStyleTipDiv").css(({'color': appModel.get('color_message_text')}))
        this.$el.find("#firstOptionStyleDiv").css({'background-color': appModel.get('color_message_background')})
        this.$el.find("#firstOptionStyleTipDiv").css({'font-weight': appModel.get('message_bold')})
        this.$el.find("#firstOptionStyleTipDiv").css({'font-family': appModel.get('message_font')})


        if (TipsGlobModel.myTipCollection.length != 0) {

        TipsGlobModel.myTipCollection.each((obj) => {
            console.log(obj.toJSON())
            if (obj.get('date') == '')
                this.$el.find("#tipsTable").append("<tr><td>" + obj.get('serial') + "</td><td>" + obj.get('text') + "</td></tr>")
            else  this.$el.find("#tipsTable").append("<tr><td>" + obj.get('date') + "</td><td>" + obj.get('text') + "</td></tr>")


        })
        }
        // else if (TipsGlobModel.myDateTipCollection.length != 0) {
        //     TipsGlobModel.myDateTipCollection.each((obj) => {
        //
        //     })
        // }

        this.$el.find("#logo").attr('src', appModel.get('logo'));
    }

    show() {
        $("body").append(this.el);
        this.modal.modal("show");
        this.modal.one("hidden.bs.modal", this.remove.bind(this));
    }

    close() {
        this.modal.modal("hide");
    }

    remove() {
        Backbone.View.prototype.remove.call(this);
        this.trigger("close", this);
    }


    render() {

    }


}
