/**
 * Created by Lior on 03-Nov-16.
 */
import * as Backbone from "backbone";
import * as Handlebars from "handlebars/dist/handlebars"
import mycollection from "../model/AppModel"
import Helper from "../../jqueryMore";

export default class ThirdStepApp extends Backbone.View {
    constructor(appModel) {
        super({
            events: {
                //  "keyup .nameField": "nameChanged",
            }
        });

        this.$el.html(Helper.uploadTpl("thirdStepApp"));

        this.$el.find('div.product-chooser-item').removeClass('selected');

        this.$el.find('div.product-chooser-item').each(function () {
            if ($(this).attr('id') == appModel.get('design_type')) {
                $(this).addClass('selected');
                $(this).find('input[type="radio"]').prop("checked", true);
            }
        })


        var that = this
        this.$el.find('div.product-chooser').not('.disabled').find('div.product-chooser-item').on('click', function () {
            $(this).parent().parent().find('div.product-chooser-item').removeClass('selected');
            that.selectionId = $(this).attr('id');
            appModel.set({design_type: that.selectionId})
            $(this).addClass('selected');
            $(this).find('input[type="radio"]').prop("checked", true);

        });


    }

    render() {
    }


}


$(function () {

});


//
//
// $(function () {
//     $('div.product-chooser').not('.disabled').find('div.product-chooser-item').on('click', function () {
//         $(this).parent().parent().find('div.product-chooser-item').removeClass('selected');
//         $(this).addClass('selected');
//         $(this).find('input[type="radio"]').prop("checked", true);
//
//     });
// });