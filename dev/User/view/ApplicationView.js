import * as Backbone from "backbone";
import Helper from "../../jqueryMore";
import * as Handlebars from "handlebars/dist/handlebars"
// import Vue from 'vue'
//import mycollection from "../model/AppModel"
import AppsGlobModel from "../model/AppModel"
// import SummaryStepModalView from "../view/SummaryStepModalView"
import SummaryStepModalView from "../view/SummaryStepModalView"

import {default as swal} from 'sweetalert2';
export class ApplicationView extends Backbone.View {
    constructor(appModel) {
        super({
            events: {
                // "click #quickViewBtn": "onQuickViewBtnClick"
            }
        })
        this.appModel = appModel;
        this.render(this.appModel);
        this.serial = this.appModel.get('serial');
        this.$el.find("#deleteAppBtn").on('click', function (e) {
            swal({
                title: 'האם אתה בטוח?',
                text: "לא תוכל לשחזר!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'אישור',
                cancelButtonText: 'לא!, בטל',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false
            }).then(function () {


                this.appModel.destroy();
                this.remove();

                swal(
                    'בוצע',
                    'האפליקפציה נמחקה',
                    'success'
                )
            }.bind(this), function (dismiss) {
                // dismiss can be 'cancel', 'overlay',
                // 'close', and 'timer'
                if (dismiss === 'cancel') {
                    swal(
                        'בוטל',
                        'סמידע שלך מוגן:)',
                        'error'
                    )
                }
            })
        }.bind(this));
        this.$el.find("#quickViewBtn").on('click', ()=> {
            console.log("נעשה")
            this.modalView = new SummaryStepModalView(appModel);
            this.modalView.show();
        })

    }

    onQuickViewBtnClick() {
        // alert("dfdfdfdf")
        this.modalView = new SummaryStepModalView(appModel);
        this.modalView.show();
    }

    render(appModel) {

        let appmodeljson = this.appModel.toJSON();
        let han = Handlebars.compile(Helper.uploadTpl("appContainer"));
        this.$el.append(han(appmodeljson));

        this.editAppBtn = this.$el.find("#editAppButton");
        this.editAppLink = this.$el.find("#editAppLink");
        this.appContanier = this.$el.find("#appContanier");
        this.upSection = this.$el.find("#upSection");

        this.editAppBtn.click(() => {
            AppsGlobModel.nowApp.set({nowAppId: this.serial})
            Backbone.history.loadUrl('createApp');
        });
        this.upSection.click(() => {
            //AppsGlobModel.nowApp.set({nowAppId : this.appId})
            window.app.navigate("createApp/" + this.serial, {trigger: true, replace: true})
            //    Backbone.history.loadUrl('createApp/'+this.appId);
        });
        return this;
    }
}
export default ApplicationView;
//
// var appCollection = Backbone.collection.extend({
//     model: AppModel
// })