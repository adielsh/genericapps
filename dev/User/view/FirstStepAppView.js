/**
 * Created by Adiel on 30/10/2016.
 */
import * as Backbone from "backbone";
import * as Handlebars from "handlebars/dist/handlebars"
import myAppsCollection from "../model/AppModel"
import Helper from "../../jqueryMore";
import AppsGlobModel from "../model/AppModel"
import {default as swal} from 'sweetalert2';


export default class FirstStepApp extends Backbone.View {
    constructor(appModel, doesNewApp) {
        super({
            events: {
                //  "keyup .nameField": "nameChanged",
            }
        });


        this.$el.html(Helper.uploadTpl("firstStepApp"));

        this.$el.find("#firstOpTitleTextLabel").val(appModel.get('name'))
        this.$el.find("#nameText").html(appModel.get('name'))
        this.$el.find("#comment").val(appModel.get('description'))
        this.$el.find("#nameField").val(appModel.get('name'))

        ;
        this.saveFirstStepBtn = this.$el.find("#saveFirstStepBtn")
        if (doesNewApp == false) {
            this.saveFirstStepBtn.hide()
        }

        else {
            this.saveFirstStepBtn.show()
        }


        this.$el.find("#nameField").on('keyup', ()=> {
            // console.log("sdf")

            this.$el.find("#counterName").text(10 - this.$el.find("#nameField").val().length + " תווים נשארו ")
        })

        this.saveFirstStepBtn.on('click', function () {
            if (this.$el.find("#nameField").val() == '') {

                swal({
                    title: 'לא הכנסת שם!',
                    text: 'הכנס שם בבקשה',
                    timer: 500
                }).then(
                    function () {
                    },
                    // handling the promise rejection
                    function (dismiss) {
                        if (dismiss === 'timer') {
                            console.log('I was closed by the timer')
                        }
                    }
                )
            }

            else {
                AppsGlobModel.myAppsCollection.add(appModel)
                console.log("savedOnCollection")
                swal(
                    'אפליקציה נוספה בהצלחה',
                    '',
                    'success'
                )
            }


        }.bind(this))


        var that = this

        this.$el.find("#nameField").on('input', function (e) {
            that.$el.find("#nameText").html(that.$el.find("#nameField").val()).addClass("firstStepAppTextLabel");
            appModel.set({name: that.$el.find("#nameField").val()})

        });
        this.$el.find("#nameField").on('focus', function (e) {
            that.$el.find("#nameText").html(that.$el.find("#nameField").val()).addClass("firstStepAppTextLabel");


        });
        this.$el.find("#nameField").on('focusout', function (e) {
            that.$el.find("#nameText").html(that.$el.find("#nameField").val()).removeClass("firstStepAppTextLabel");


        });

        this.$el.find("#comment").on('input', function (e) {
            that.$el.find("#nameText").html(that.$el.find("#nameField").val());
            appModel.set({description: that.$el.find("#comment").val()})
        });


        this.$el.find("#goToSecStep").on('click', function () {
            AppsGlobModel.myAppsCollection.add(appModel)

        })


        /**************Upload logo Section******************/
        this.$el.on('change', '.btn-file :file', function () {
            var input = $(this), label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
            input.trigger('fileselect', [label]);
        });

        this.$el.find('.btn-file :file').on('fileselect', function (event, label) {
            var input = that.$el.find('.input-group:text'), log = label;
            if (input.length) {
                input.val(log);
            } else {
                if (log) {
                }
            }

        });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    //   console.log(e.target.result)
                    appModel.set({'logo': e.target.result})
                    // console.log(appModel.get('logo'))
                    that.$el.find('#img-upload').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        this.$el.find("#imgInp").change(function () {
            readURL(this);
        });
        this.$el.find("#img-upload").attr("src", appModel.get('logo'));
        console.log(this.$el.find("#img-upload").attr("src"))
        /**************End Upload logo Section***************/

    }

    render() {
    }


}
