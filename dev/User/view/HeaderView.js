import * as Backbone from "backbone";
//import * as BackboneModal from "backbone-modal";
import Helper from "../../jqueryMore";
//import AppRouter from "../../AppRouter"
import AppsGlobModel from "../model/AppModel"
import EditUserModalView from "../view/EditUserModalView"
import appRouter from "../../app"


import {default as swal} from 'sweetalert2';
window.js = window.js || {};
js.view = js.view || {};


export  class HeaderView extends Backbone.View {
    constructor(eventBus) {
        super({

            events: {
                "click #addAppButton": "onAddButtonClick",
                "click #mainPageBtn": "onMainPageBtnClick",
                "click #userDataBtn": "onUserDataBtnClick",

            },
        });
        this.id  = "headerPage"
        this.eventBus = eventBus;


        this.addAppButton = this.$el.find("#addAppButton")
        this.mainPageButton = this.$el.find("#mainPageBtn")


        this.$el.html(Helper.uploadTpl("header"));
        // this.addAppBtn = this.$el.find("#addAppButton");
        // this.mainPageBtn = this.$el.find("#mainPageBtn");


        //     .click(function(){
        //     console.log("headerbutnon clicked")
        //   //  goTo("mainPage");
        // });




        // this.addAppBtn.click(() => {
        //     console.log("trying navigate from header")
        //     // AppRouter.navigate('createApp', {
        //     //     trigger: true,
        //     //     replace: true
        //     // });
        //
        //     Backbone.history.loadUrl('createApp');
        //     //Helper.goTo("mainPage")
        // });
        // this.mainPageBtn.click(() => {
        //     Backbone.history.loadUrl('mainPage',);
        // });

    }
    onAddButtonClick () {

       // this.addAppButton.attr("href", "#createApp")
        // console.log("on header press  ")
        // Backbone.history.loadUrl('createApp/-1');
        //window.app.navigate("createApp",{trigger:true,replace:true})

        console.log("after header pressed, still in header view file  ")
        window.app.navigate("createApp",{trigger:true,replace:true})

    }
    onMainPageBtnClick () {
        //You can use the root Backbone object as a global event bus
        //Publish a message
        // console.log("on header press ")
        // this.mainPageButton.attr("href", "#mainPage/-1")

        window.app.navigate("mainPage",{trigger:true,replace:true})



        // if(AppsGlobModel.nowApp.get('inProccess')==0) {
        //     console.log("do op to change to mainapp");
        //
        //
        //
        //
        //
        //     Backbone.history.loadUrl('mainPage');
        // }
        // else{
        //
        //     swal({
        //         title: 'בטוח?',
        //         text: " אם התחלת ליצור אפליקציה חדשה - היא לא תישמר!",
        //         type: 'warning',
        //         showCancelButton: true,
        //         confirmButtonColor: '#3085d6',
        //         cancelButtonColor: '#d33',
        //         confirmButtonText: 'אישור',
        //         cancelButtonText: 'חזור',
        //         confirmButtonClass: 'btn btn-success',
        //         cancelButtonClass: 'btn btn-danger',
        //         buttonsStyling: false
        //     }).then(function() {
        //         Backbone.history.loadUrl('mainPage');
        //         swal(
        //             'עברת לדף תצוגה!',
        //             'שכוייח',
        //             'success'
        //         )
        //     }, function(dismiss) {
        //         // dismiss can be 'cancel', 'overlay',
        //         // 'close', and 'timer'
        //         if (dismiss === 'cancel') {
        //             swal(
        //                 'בוטל',
        //                 ':)',
        //                 'error'
        //             )
        //         }
        //     })
        //
        //     // if (confirm('אם אתה יוצר אפליקציה חדשה, השינויים לא יישמרו, מאשר?')) {
        //     //     // do operation
        //     //     Backbone.history.loadUrl('mainPage');
        //     // }
        //
        // }
       // Backbone.trigger('mainBtnPressed');
      //  Backbone.trigger('header:mainBtn');
    }

    onUserDataBtnClick(){
       // alert("dfdfdfdf")
         this.modalView = new EditUserModalView();
         this.modalView.show();
    }

}
