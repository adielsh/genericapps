/**
 * Created by Adiel on 30/10/2016.
 */
import * as Backbone from "backbone";
import * as Handlebars from "handlebars/dist/handlebars"
import myAppsCollection from "../model/AppModel"
import Helper from "../../jqueryMore";
import AppsGlobModel from "../model/AppModel"
import {default as swal} from 'sweetalert2';
import {default as spectrum} from 'spectrum-colorpicker';


export default class FirstStepApp extends Backbone.View {
    constructor(appModel) {
        super({
            events: {
                //  "keyup .nameField": "nameChanged",
            }
        });
        this.render(appModel)
        this.uploadPic(appModel)

    }

    designTypeChecking(appModel) {
        switch (appModel.get('design_type')) {


            case "1":

                this.$el.find("#firstOptionStyleTitleDiv").removeClass();
                this.$el.find("#firstOptionStyleTitleDiv").addClass("firstTitle");


                this.$el.find("#firstOptionStyleTipDiv").removeClass();
                this.$el.find("#firstOptionStyleTipDiv").addClass("firstTip");


                this.$el.find("#firstOptionStyleAdsDiv").removeClass();
                this.$el.find("#firstOptionStyleAdsDiv").addClass("firstAds");

                break;

            case "2":

                this.$el.find("#firstOptionStyleTitleDiv").removeClass();
                this.$el.find("#firstOptionStyleTitleDiv").addClass("secTitle");


                this.$el.find("#firstOptionStyleTipDiv").removeClass();
                this.$el.find("#firstOptionStyleTipDiv").addClass("secTip");


                this.$el.find("#firstOptionStyleAdsDiv").removeClass();
                this.$el.find("#firstOptionStyleAdsDiv").addClass("secAds");


                break;
            case "3":
                this.$el.find("#firstOptionStyleTitleDiv").removeClass();
                this.$el.find("#firstOptionStyleTitleDiv").addClass("thirdTitle");


                this.$el.find("#firstOptionStyleTipDiv").removeClass();
                this.$el.find("#firstOptionStyleTipDiv").addClass("thirdTip");


                this.$el.find("#firstOptionStyleAdsDiv").removeClass();
                this.$el.find("#firstOptionStyleAdsDiv").addClass("thirdAds");

                break;

            default:
                break;
        }

    }

    render(appModel) {
        console.log("render again")
        this.$el.html(Helper.uploadTpl("designStepApp"));

        let that = this;
        this.$el.find('#radioBtn a').on('click', function () {
            var sel = $(this).data('title');
            var tog = $(this).data('toggle');
            $('#' + tog).prop('value', sel);

            $('a[data-toggle="' + tog + '"]').not('[data-title="' + sel + '"]').removeClass('active').addClass('notActive');
            var selc = $('[data-title="' + sel + '"]').attr("some")
            console.log(selc)
            // sel = $('a[data-toggle="' + tog + '"]').not('[data-title="' + sel + '"]').val();
            appModel.set({design_type: selc})
            $('a[data-toggle="' + tog + '"][data-title="' + sel + '"]').removeClass('notActive').addClass('active');
            that.designTypeChecking(appModel);
            // this.render(appModel)
        })


        this.designTypeChecking(appModel);

        this.$el.find("#firstOpTitleTextLabel").val(appModel.get('title'))
        this.$el.find("#titleField").html(appModel.get('title'))

        //for title text/////////////////////////////
        this.$el.find("#titleField").on('input', function (e) { //text title
            this.$el.find("#firstOptionStyleTitleDiv").html(this.$el.find("#titleField").val())
            appModel.set({title: this.$el.find("#titleField").val()})
        }.bind(this));
        this.$el.find("#firstOptionStyleTitleDiv").html(appModel.get('title'))
        this.$el.find("#titleField").val(appModel.get('title'));


        this.$el.find("#titleField").on('keyup', ()=> {

            this.$el.find("#nameCounter").text(15 - this.$el.find("#titleField").val().length + " תווים נשארו ")
        })


        //for title color text/////////////////////////////
        this.$el.find("#titleColorField").spectrum({
            color: appModel.get('color_title_text'),
            move: function (color) {

                this.$el.find("#firstOptionStyleTitleDiv").css({'color': color.toHexString()})
                appModel.set({color_title_text: color.toHexString()})

            }.bind(this),


        });
        this.$el.find("#firstOptionStyleTitleDiv").css(({'color': appModel.get('color_title_text')}))


        //for title  background color///////////////////////
        this.$el.find("#titleBackgroundColorField").spectrum({
            color: appModel.get('color_title_background'),
            move: function (color) {

                this.$el.find("#firstOptionStyleTitleDiv").css({'background-color': color.toHexString()})
                appModel.set({color_title_background: color.toHexString()})

            }.bind(this),

        });
        this.$el.find("#firstOptionStyleTitleDiv").css(({'background-color': appModel.get('color_title_background')}))

        //for title font//////////////
        this.$el.find("#titleFontSelect").on('change', function (e) { //title font
            this.$el.find("#firstOptionStyleTitleDiv").css({'font-family': this.$el.find("#titleFontSelect option:selected").text()})
            appModel.set({title_font: this.$el.find("#titleFontSelect option:selected").val()})

        }.bind(this));

        //for title bold/////////////////
        this.$el.find("#titleBoldCheckBox").on('change', function (e) { //title bold
            if (this.$el.find("#titleBoldCheckBox").is(":checked")) {
                this.$el.find("#firstOptionStyleTitleDiv").css({'font-weight': 'bold'})
                appModel.set({title_bold: 'bold'})
                console.log(appModel.get('title_bold'))

            }
            else {
                this.$el.find("#firstOptionStyleTitleDiv").css({'font-weight': 'normal'})
                appModel.set({title_bold: 'normal'})
                console.log(appModel.get('title_bold'))

            }
        }.bind(this))

        this.$el.find("#firstOptionStyleTitleDiv").css({'font-weight': appModel.get('title_bold')})

        if (appModel.get('title_bold') == 'bold') {
            this.$el.find("#titleBoldCheckBox").prop('checked', true);
        }
        else {
            this.$el.find("#titleBoldCheckBox").prop('checked', false);

        }


        /******************TIP SECTION******************************/
        //for tip text//////////////////////////
        this.$el.find("#tipTextColorField").spectrum({
            color: appModel.get('color_message_text'),
            move: function (color) {

                this.$el.find("#firstOptionStyleTipDiv").css({'color': color.toHexString()})
                appModel.set({color_message_text: color.toHexString()})

            }.bind(this),

        });
        this.$el.find("#firstOptionStyleTipDiv").css(({'color': appModel.get('color_message_text')}))


        //for tip background///////////////////
        this.$el.find("#tipBackgroundColorField").spectrum({
            color: appModel.get('color_message_background'),
            move: function (color) {

                this.$el.find("#firstOptionStyleDiv").css({'background-color': color.toHexString()})
                appModel.set({color_message_background: color.toHexString()})

            }.bind(this),

        });
        this.$el.find("#firstOptionStyleDiv").css({'background-color': appModel.get('color_message_background')})


        //for tip font//////////////
        this.$el.find("#tipFontSelect").on('change', function (e) { //title font
            this.$el.find("#firstOptionStyleTipDiv").css({'font-family': this.$el.find("#tipFontSelect option:selected").text()})
            appModel.set({message_font: this.$el.find("#tipFontSelect option:selected").val()})

        }.bind(this));
        this.$el.find("#firstOptionStyleTipDiv").css({'font-family': appModel.get('message_font')})


        //for tip bold/////////////////
        this.$el.find("#tipBoldCheckBox").on('change', function (e) { //title bold
            if (this.$el.find("#tipBoldCheckBox").is(":checked")) {
                this.$el.find("#firstOptionStyleTipDiv").css({'font-weight': 'bold'})
                appModel.set({message_bold: 'bold'})
                console.log(appModel.get('message_bold'))

            }
            else {
                this.$el.find("#firstOptionStyleTipDiv").css({'font-weight': 'normal'})
                appModel.set({message_bold: 'normal'})
                console.log(appModel.get('message_bold'))

            }
        }.bind(this))

        this.$el.find("#firstOptionStyleTipDiv").css({'font-weight': appModel.get('message_bold')})

        if (appModel.get('message_bold') == 'bold') {
            this.$el.find("#tipBoldCheckBox").prop('checked', true);
        }
        else {
            this.$el.find("#tipBoldCheckBox").prop('checked', false);

        }

    }

    uploadPic(appModel) {
        let that = this

        /**************Upload logo Section******************/
        this.$el.on('change', '.btn-file :file', function () {
            var input = $(this), label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
            input.trigger('fileselect', [label]);
        });

        this.$el.find('.btn-file :file').on('fileselect', function (event, label) {
            var input = that.$el.find('.input-group:text'), log = label;
            if (input.length) {
                input.val(log);
            } else {
                if (log) {
                }
            }

        });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    //   console.log(e.target.result)
                    appModel.set({'logo': e.target.result})
                    // console.log(appModel.get('logo'))
                    that.$el.find('#img-upload').css("background-image", "url(e.target.result)");
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        this.$el.find("#imgInp").change(function () {
            readURL(this);
        });
        this.$el.find("#img-upload").attr("src", appModel.get('logo'));
        console.log(this.$el.find("#img-upload").attr("src"))
        /**************End Upload logo Section***************/


    }


}
