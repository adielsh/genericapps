/**
 * @author elad
 */


Array.prototype.random = function () {
	var num = this.length;
	var random = Math.floor(Math.random() * num);
	return this[random];

};
Array.prototype.remove = function (obj) {
	var index = this.indexOf(obj);
	if (index > -1) {
		this.splice(index, 1);
		return true;
	}
	return false;
};
Array.prototype.shuffle = function () {
	var j, x, i;
	for (i = this.length; i; j = parseInt(Math.random() * i), x = this[--i], this[i] = this[j], this[j] = x)
		;
	return this;
};
Array.prototype.clone = function () {
	return this.slice(0);
};

function getDate() {
	return new Date().toJSON().slice(0, 10);
}
function getTime() {
	var timeObj = new Date();
	return timeObj.getHours() + ":" + timeObj.getMinutes();
}
function isDevice() {
	return (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent));
}
function isOpera() {
	// Opera 8.0+ (UA detection to detect Blink/v8-powered Opera)
	return !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;
}
function isFirefox() {

	return typeof InstallTrigger !== 'undefined';   // Firefox 1.0+
}
function isSafari() {

	return Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0;
}
function isChrome() {

	return !!window.chrome && !isOpera();
}
function isIE() {
	return /*@cc_on!@*/false || !!document.documentMode;
}

function isEmpty(par) {
	return (par === undefined || par === null || (typeof par === "string" && par.trim() === "") || (par === 0) || (Array.isArray(par) && par.length === 0));
}

function getBrowser() {
	if (isOpera()) {
		return "opera";
	} else if (isFirefox()) {
		return "firefox";
	} else if (isChrome()) {
		return "chrome";
	} else if (isIE()) {
		return "ie";
	} else if (isSafari()) {
		return "safari";
	}
}
function canvasToImage(canvas, callback) {
	var image = new Image();
	image.src = canvas.toDataURL("image/png");
	if (typeof (callback) == "function") {
		image.onload = callback.bind(this, image);
	}
	return image;
}
function downloadCanvasImage(canvas) {
	canvasToImage(canvas, function (image) {
		if (!isIE()) {
			var a = $("<a>")
				.attr("href", image.src)
				.attr("download", "img.png")
				.appendTo("body");
			a[0].click();
			a.remove();
		} else {
			alert("Right-click on the game and 'sava image as'");
		}
	});
}
;


function getQueryParams() {
	// This function is anonymous, is executed immediately and
	// the return value is assigned to QueryString!
	var query_string = {};
	var query = window.location.search.substring(1);
	var vars = query.split("&");
	for (var i = 0; i < vars.length; i++) {
		var pair = vars[i].split("=");
		// If first entry with this name
		if (typeof query_string[pair[0]] === "undefined") {
			query_string[pair[0]] = pair[1];
			// If second entry with this name
		} else if (typeof query_string[pair[0]] === "string") {
			var arr = [query_string[pair[0]], pair[1]];
			query_string[pair[0]] = arr;
			// If third or later entry with this name
		} else {
			query_string[pair[0]].push(pair[1]);
		}
	}
	return query_string;
}
;

(function ($) {
	// 	get all attributes of element

	$.fn.getAttributes = function () {
		var attributes = {};

		if (this.length) {
			$.each(this[0].attributes, function (index, attr) {
				attributes[attr.name] = attr.value;
			});
		}

		return attributes;
	};

	$.fn.setAttributes = function (data) {
		for (prop in data) {
			this.attr(prop, data[prop]);
		}
		return this;
	};

	// Checks if the element

	$.fn.isset = function () {
		if (this.length > 0) {
			return true;
		} else {
			return false;
		}
	};

	// 	set value in input element from obj data

	$.fn.updateForm = function (data) {
		var self = this;
		var inputs = this.find(":input");
		inputs.each(function (i) {
			var $this = $(this);
			if (data[this.name] != undefined) {
				if (this.type == "file") {
					return;
				}
				else if (this.type == "checkbox" || this.type == "radio") {
					$this.prop("checked", ($this.val() == data[this.name]));
				} else {
					$this.val(data[this.name]);
				}
			}
		});
	};

	$.fn.playDisabled = function () {
		this.disabled();
		this.append('<span class="glyphicon glyphicon-refresh glyphicon-refresh-animate">');
	};

	$.fn.send = function (options) {
		var self = this;
		var opt = {};
		this.submit(function (e) {
			e.preventDefault();
			var hasFile = $(self).find("input[type='file']").isset();
			if (hasFile) {
				opt.data = new FormData(self[0]);
				opt.async = false, opt.cache = false;
				opt.contentType = false;
				opt.processData = false;
			} else {
				opt.data = self.serialize();
			}
			opt.onsubmit = function () {
				//		console.log("submit start");
			};
			opt.method = "post";
			opt.action = location.href;
			opt.success = function (data) {
			};
			opt.fail = function (a, b, c) {
				console.warn(a, b, c);
			};
			$.extend(opt, self.getAttributes());
			$.extend(opt, options);

			opt.url = opt.action;
			opt.onsubmit(e);
			$.ajax(opt);

		});
	};
	$.fn.sendNow = function (options) {
		var self = this;
		var opt = {};

		var hasFile = $(self).find("input[type='file']").isset();
		if (hasFile) {
			opt.data = new FormData(self[0]);
			opt.async = false, opt.cache = false;
			opt.contentType = false;
			opt.processData = false;
		} else {
			opt.data = self.serialize();
		}
		opt.onsubmit = function () {
			//		console.log("submit start");
		};
		opt.method = "post";
		opt.action = location.href;
		opt.success = function (data) {
			console.log(data);
		};
		opt.fail = function (a, b, c) {
			console.warn(a, b, c);
		};
		$.extend(opt, self.getAttributes());
		$.extend(opt, options);

		opt.url = opt.action;
		$.ajax(opt);

	};
	$.fn.brothers = function (q) {
		var $this = this;
		return this.parent().children(q).not(this);
	};
	$.fn.getInputObj = function (q) {
		var obj = {},
			inputs;
		var query = (q != undefined) ? q : ":input";
		inputs = this.find(query);
		inputs.each(function () {
			if (this.name == "")
				return;
			if (this.value == "") {
				obj[this.name] = null;
				return;
			}
			var $this = $(this);
			obj[this.name] = $this.realVal();
		});
		return obj;
	};

	$.fn.cdata = function (text) {
		this.html("<![CDATA[" + text + "]]>");
	}
	jQuery.createElement = function (obj) {
		var element;
		var defaultprop = {
			element: "div",
		}
		var data = $.extend(defaultprop, obj);
		element = $("<" + data.element + "/>");
		delete data.element;
		if (data.html != undefined) {
			element.html(data.html);
			delete data.html;
		}
		element.setAttributes(data);
		return element;
	}

	jQuery.r = function (e) {
		var j = jQuery(e);
		j.data('old-state', j.html());
		return j;
	};

	jQuery.fn.saveHtml = function (e) {
		this.data('old-state', this.html());
		return this;
	};

	jQuery.fn.restart = function () {
		this.html(this.data('old-state'));
		return this;
	};
	jQuery.fn.tagName = function () {
		return this.prop("tagName");
	};

	jQuery.fn.disabled = function (bol, text) {
		var b = (bol !== undefined) ? bol : true;
		var btn = this;
		if (this.is("form")) {
			btn = this.find(":input[type=submit]");
		}
		btn.prop("disabled", b);
		if (text != undefined) {
			btn.text(text);
		}

	};
	jQuery.fn.required = function (bol) {
		var b = true;
		if (bol !== undefined) {
			b = bol;
		}
		this.prop("required", b);
		return this;
	};

	jQuery.fn.realVal = function () {
		var $obj = $(this);
		var val = this.val();
		var type = this.attr('type');
		if (type && type === 'checkbox') {
			var un_val = $obj.attr('data-unchecked');
			if (typeof un_val === 'undefined')
				un_val = "off";
			return $obj.prop('checked') ? val : un_val;
		} else if (type && type === 'radio') {
			var e = $obj.prop("checked");
			if ($obj.prop("checked")) {
				return val;
			} else {
				return null;
			}
		} else {
			return val;
		}
	};


})(jQuery);
function goHomePage() {
	app.navigate('', {
		trigger: true,
		replace: true
	});
}

function goTo(to) {
	app.navigate(to, {
		trigger: true,
		replace: true
	});
}

function uploadTpl(tmpl_name) {
	if (!uploadTpl.tmpl_cache) {
		uploadTpl.tmpl_cache = {};
	}

	if (!uploadTpl.tmpl_cache[tmpl_name]) {
		var tmpl_dir = 'tpl';
		var tmpl_url = tmpl_dir + '/' + tmpl_name + '.html';

		var tmpl_string;
		$.ajax({
			url: tmpl_url,
			method: 'GET',
			async: false,
			cache: false,
			dataType: "html",
			success: function success(data) {
				tmpl_string = data;
			}
		});
		uploadTpl.tmpl_cache[tmpl_name] = tmpl_string;
	}
	return uploadTpl.tmpl_cache[tmpl_name];
}

var __extends = function __extends(d, b) {
	for (var p in b) {
		if (b.hasOwnProperty(p)) d[p] = b[p];
	}
	function __() {
		this.constructor = d;
	}

	d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};

const Helper = {uploadTpl,goTo};
// Helper.uploadTpl = uploadTpl;
export default Helper;