var webpack = require("webpack");
var path = require("path");
module.exports = {
	entry: ['babel-polyfill', "./dev/app.js"],
	output: {
		path: __dirname + '/',
		publicPath: "./dist/",
		filename: "app.js",
		sourceMapFilename: "app.js.map"
	},
	module: {
		loaders: [
			{test: /\.js$/, exclude: /node_modules/, loader: "babel-loader"}

		]
	},
	devtool: "source-map",

	plugins: [
		new webpack.ProvidePlugin({
			$: "jquery",
			jQuery: "jquery"
		}),

		new webpack.ProvidePlugin({
			swal: "sweetalert",
			use_css: true, // false for someone need custom theme
			use_js: true
		})
		// new webpack.optimize.UglifyJsPlugin({
		// 	compress: {
		// 		warnings: false,
		// 	},
		// 	comments: false
		// })
	]
};
/**
 * Created by Dell on 20 יולי 2016.
 */
